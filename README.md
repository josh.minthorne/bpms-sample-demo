# BPMS Sample Demo

# Description
Simple BPMS Sample Demo to demonstrate CI/CD integration with BPM Suite.

# BPM Best Practices
- Use development branch for developers looking to make changes to the BPM projects
    - Each dev push changes there and BPM will handle the merging
- Master branch is only for releases which go to the BPM Runtime environments
- Artifact manager needs to have a proxy to be able to pull dependencies from Red Hat for BPM builds
- Change the default BPM .m2/settings.xml to have a artifact manager profile with a repository and pluginRepository pointed to your artifact manager (ex. Nexus) and add it as an active profile
    - This allows BPM to be able to pull repositories from our public repositories in Nexus
- In the BPM project, pom.xml needs to be altered for working with the continuous integration server to piece everything together but BPM works without it (can detail changes if desired)
- Optional (for BPM Designer Suite only if desired):
    - In BPM at bpms/jboss-eap-7.0/bin/.niogit/ add in git hooks to your hosted git repository for projects to sync when commits are pushed to it from developers
    - Or create default git hooks and then in the BPMS standalone.xml (bpms/jboss-eap-7.0/standalone/configuration/standalone.xml) add in a property in system properties like this: <property name="org.uberfire.nio.git.hooks" value="location/to/hooks/directory/hooks"/> so all new projects created/cloned into this BPM will have those githooks associated with it
